<?php
namespace Iss\Api\Exception;

use Iss\Api\Messaging\Response\Error;

class ApiException extends \Exception
{
    private Error $error;

    public function __construct(Error $error)
    {
        parent::__construct($error->detail, $error->status);
        $this->error = $error;
    }

    public function getResponse()
    {
        return $this->error;
    }
}