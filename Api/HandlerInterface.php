<?php
namespace Iss\Api;

use Iss\Api\Messaging\Request;

interface HandlerInterface
{
    public function getCollection(Request $request);

    public function getResource(Request $request);

    public function post(Request $request);

    public function put(Request $request);

    public function patch(Request $request);

    public function delete(Request $request);
}