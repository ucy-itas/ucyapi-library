<?php

namespace Iss\Api\Iterator;


class ApiFilterIterator extends \FilterIterator
{
    private $userFilter;

    public function __construct(\Iterator $iterator , $filter )
    {
        parent::__construct($iterator);
        $this->userFilter = $filter;
    }

    public function accept()
    {
        $data = $this->getInnerIterator()->current();
        return $this->userFilter($data);
    }
}