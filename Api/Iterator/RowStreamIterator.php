<?php

namespace Iss\Api\Iterator;


class RowStreamIterator extends \IteratorIterator
{
    public function current()
    {
        $row = parent::current();
        if (is_array($row)) {
            foreach ($row as $column_name => $value) {
                if (is_resource($value) && (get_resource_type($value) === 'stream')) {
                    $row[$column_name] = stream_get_contents($value);
                }
            }
        }
        return $row;
    }
}