<?php
namespace Iss\Api\Messaging;

use Iss\Api\Messaging\Response\Error;

class Response extends \stdClass implements \JsonSerializable
{
    public string $id = '';
    public string $status = '';
    public string $title = '';

    public $data;

    public function __construct($object = null)
    {
        if (!is_null($object)) {
            $this->addObject($object);
        }
    }

    public function addObject($object)
    {
        $this->data = $object;
        return $this;
    }

    public function jsonSerialize()
    {
        return ['data' => $this->data ?? null];
    }

    public function isEmpty(): bool
    {
        if(!isset($this->data) or is_null($this->data)) return true;
        return false;
    }
}
