<?php

namespace Iss\Api\Messaging\Request;

use Swaggest\JsonSchema\Schema;

class Variable implements \JsonSerializable
{
    /**
     * @var string[] $target
     */
    protected array $target;

    protected ?Schema $schema = null;

    public function __construct(protected string $name, protected string $type = 'string', string|array|null $target = null, ?Schema $schema = null)
    {
        $this->setTarget(is_null($target) ? [$name] : $target);
        if (!is_null($schema)) {
            $this->setSchema($schema);
        }
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type)
    {
        $this->type = $type;
    }

    public function getTarget(): array
    {
        return $this->target;
    }

    public function setTarget(string|array $target)
    {
        if (is_array($target)) {
            $this->target = $target;
        } else {
            $this->target = [$target];
        }
    }

    public function setSchema(Schema $schema)
    {
        $this->schema = $schema;
        $type = $schema->type;
        if (is_string($type) && $type) {
            $this->setType($type);
        }
    }

    public function hasSchema(): bool
    {
        return !is_null($this->schema);
    }

    public function getSchema(): ?Schema
    {
        return $this->schema;
    }

    public function jsonSerialize()
    {
        return [
            'name' => $this->getName(),
            'type' => $this->getType(),
            'target' => $this->getTarget()
        ];
    }
}