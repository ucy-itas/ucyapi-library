<?php

namespace Iss\Api\Messaging\Request;


class Attribute implements \JsonSerializable
{
    protected string $name;

    protected string $type;

    protected string $target;

    protected ?string $callback;

    protected array $sub_attributes = [];

    public static function fromArray(array $params) : Attribute
    {
        $type = $params['type'] ?? 'string';
        $attribute = new Attribute(
            $params['name'] ?? '',
            $type,
            $params['target'] ?? '',
            $params['callback'] ?? null);

        if ($type === 'array') {
            $items = $params['items'] ?? [];
            $child_attribute = Attribute::fromArray($items);
            $attribute->addSubAttribute($child_attribute);
        } elseif ($type === 'object') {
            $properties = $params['properties'] ?? [];
            foreach ($properties as $name => $property) {
                $property['name'] = $name;
                $child_attribute = Attribute::fromArray($property);
                $attribute->addSubAttribute($child_attribute);
            }
        }
        return $attribute;
    }

    public function __construct(string $name, ?string $type = 'string', $target = null, ?string $callback = null)
    {
        $this->name = $name;
        $this->type = $type ?? 'string';
        if (is_null($target)) {
            $this->target = $name;
        } elseif (is_string($target)) {
            $this->target = $target;
        } elseif (is_array($target)) {
            $this->target = $name;
            foreach ($target as $subname => $subattribute) {
                $child_attribute = new Attribute($subname, $subattribute['type']??null, $subattribute['target']??null, $subattribute['callback']??null);
                $this->addSubAttribute($child_attribute);
            }
        } else {
            $this->target = $target;
        }
        $this->callback = $callback;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function getTarget(): string
    {
        return $this->target;
    }

    public function setTarget($target): void
    {
        if (is_string($target)) {
            $this->target = $target;
        } elseif (is_array($target)) {
            $this->sub_attributes = [];
            foreach ($target as $subname => $subattribute) {
                $child_attribute = new Attribute($subname, $subattribute['type']??null, $subattribute['target']??null, $subattribute['callback']??null);
                $this->addSubAttribute($child_attribute);
            }
        }
    }

    public function getCallback(): ?string
    {
        return $this->callback;
    }

    public function setCallback(?string $callback): void
    {
        $this->callback = $callback;
    }

    public function jsonSerialize()
    {
        return [
            'name' => $this->getName(),
            'type' => $this->getType(),
            'target' => $this->getTarget()
        ];
    }

    public function addSubAttribute(Attribute $sub_attribute): Attribute
    {
        $this->sub_attributes[$sub_attribute->getName()] = $sub_attribute;
        return $this;
    }

    public function getSubAttributes(): array
    {
        return $this->sub_attributes;
    }
}