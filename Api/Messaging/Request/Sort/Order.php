<?php
namespace Iss\Api\Messaging\Request\Sort;

enum Order: int
{
    case ASCENDING = 1;
    case DESCENDING = 2;
}
