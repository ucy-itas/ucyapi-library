<?php
namespace Iss\Api\Messaging\Request;

use Iss\Api\Messaging\Request\Sort\Order;

class Sort implements \JsonSerializable
{
    protected Variable $variable;

    protected Order $order;

    public function __construct(string|Variable $variable, Order $order = Order::ASCENDING)
    {
        if (is_string($variable)) {
            $variable = new Variable($variable);
        }
        if ($variable instanceof Variable) {
            $this->variable = $variable;
        }
        $this->setOrder($order);
    }

    public function getOrder(): Order
    {
        return $this->order;
    }

    public function setOrder(Oder $order)
    {
        $this->order = $order;
    }

    public function getVariable(): Variable
    {
        return $this->variable;
    }

    public function jsonSerialize()
    {
        return [
            'variable' => $this->getVariable(),
            'order' => $this->getOrder()
        ];
    }
}
