<?php

namespace Iss\Api\Messaging\Request;

use Iss\Api\Exception\ApiException;
use Iss\Api\Messaging\Response\Error\BadRequest;

class Condition implements \JsonSerializable
{
    protected Variable $variable;

    protected Operator $operator;

    /**
     * @var array[string]
     */
    protected $_values = [];

    public function __construct(string|Variable $variable, $values, Operator $operator = Operator::IN)
    {
        if (is_string($variable)) {
            $variable = new Variable($variable);
        }
        if ($variable instanceof Variable) {
            $this->variable = $variable;
        } else {
            throw new \Exception("Incorrect variable type");
        }

        $this->setOperator($operator);
        if (!is_array($values)) {
            $values = [$values];
        }
        $this->setValues($values);
    }

    public function getVariable(): Variable
    {
        return $this->variable;
    }

    public function setVariable(Variable $variable): Condition
    {
        $this->variable = $variable;
        return $this;
    }

    public function getOperator(): Operator
    {
        return $this->operator;
    }

    public function setOperator(Operator $operator)
    {
        $this->operator = $operator;
    }

    public function getValues(): array
    {
        return $this->_values;
    }

    public function setValues(array $values)
    {
        $values = array_unique($values);
        sort($values);
        $this->_values = $values;
    }

    public function getValue()
    {
        return $this->_values[0] ?? null;
    }

    public function jsonSerialize()
    {
        return [
            'variable' => $this->getVariable(),
            'operator' => $this->getOperator(),
            'values' => $this->getValues()
        ];
    }

    public function apply($value): bool
    {
        switch ($this->getOperator()) {
            case Operator::IN:
            case Operator::EQUALS:
                return in_array($value, $this->getValues());
            case Operator::NOT_IN:
            case Operator::NOT_EQUALS:
                return !in_array($value, $this->getValues());
            case Operator::GREATER_THAN:
                foreach ($this->getValues() as $reference_value) {
                    if ($value <= $reference_value) {
                        return false;
                    }
                }
                return true;
            case Operator::GREATER_THAN_OR_EQUAL:
                foreach ($this->getValues() as $reference_value) {
                    if ($value < $reference_value) {
                        return false;
                    }
                }
                return true;
            case Operator::LESS_THAN:
                foreach ($this->getValues() as $reference_value) {
                    if ($value >= $reference_value) {
                        return false;
                    }
                }
                return true;
            case Operator::LESS_THAN_OR_EQUAL:
                foreach ($this->getValues() as $reference_value) {
                    if ($value > $reference_value) {
                        return false;
                    }
                }
                return true;
        }
        return false;
    }
}