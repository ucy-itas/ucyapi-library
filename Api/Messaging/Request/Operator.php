<?php

namespace Iss\Api\Messaging\Request;

enum Operator: string
{
    case IN = 'in';
    case NOT_IN = 'nin';
    case EQUALS = 'eq';
    case NOT_EQUALS = 'neq';
    case GREATER_THAN = 'gt';
    case GREATER_THAN_OR_EQUAL = 'gte';
    case LESS_THAN = 'lt';
    case LESS_THAN_OR_EQUAL = 'lte';

    function test($value, $against): bool
    {
        switch ($this) {
            case Operator::IN:
            case Operator::EQUALS:
                return in_array($value, $against);
            case Operator::NOT_IN:
            case Operator::NOT_EQUALS:
                return !in_array($value, $against);
            case Operator::GREATER_THAN:
                foreach ($against as $reference_value) {
                    if ($value <= $reference_value) {
                        return false;
                    }
                }
                return true;
            case Operator::GREATER_THAN_OR_EQUAL:
                foreach ($against as $reference_value) {
                    if ($value < $reference_value) {
                        return false;
                    }
                }
                return true;
            case Operator::LESS_THAN:
                foreach ($against as $reference_value) {
                    if ($value >= $reference_value) {
                        return false;
                    }
                }
                return true;
            case Operator::LESS_THAN_OR_EQUAL:
                foreach ($against as $reference_value) {
                    if ($value > $reference_value) {
                        return false;
                    }
                }
                return true;
        }
        return false;
    }
}
