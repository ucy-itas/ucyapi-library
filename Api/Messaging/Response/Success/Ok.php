<?php
namespace Iss\Api\Messaging\Response\Success;

use Iss\Api\Messaging\Response\Success;

class Ok extends Success
{
    public function __construct($data, string $id = null, string $title = "OK")
    {
        parent::__construct($id, "200", $title, $data);
    }
}
