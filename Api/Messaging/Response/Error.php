<?php

namespace Iss\Api\Messaging\Response;

use Iss\Api\Messaging\Response;

class Error extends Response
{
    public string $code = '';
    public string $detail = '';

    public function __construct(?string $id, ?string $status, ?string $code, ?string $title, ?string $detail)
    {
        parent::__construct();

        if ($id) {
            $this->id = $id;
        }
        if ($status) {
            $this->status = $status;
        }
        if ($code) {
            $this->code = $code;
        }
        if ($title) {
            $this->title = $title;
        }
        if ($detail) {
            $this->detail = $detail;
        }
    }

    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'status' => $this->status,
            // 'code' => $this->code,
            'title' => $this->title,
            'detail' => $this->detail
        ];
    }
}
