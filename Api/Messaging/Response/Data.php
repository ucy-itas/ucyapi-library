<?php
namespace Iss\Api\Messaging\Response;

class Data extends \stdClass
{
    public function __construct($type, $id, $attributes)
    {
        $this->id = $id;
        $this->type = $type;
        $this->attributes = $attributes;
    }
}