<?php
namespace Iss\Api\Messaging\Response\Error;

use Iss\Api\Messaging\Response\Error;

class NotFound extends Error
{
    public function __construct(string $detail, string $id = null, string $code = null, string $title = "Not Found")
    {
        parent::__construct($id, "404", $code, $title, $detail);
    }
}