<?php
namespace Iss\Api\Messaging\Response\Error;

use Iss\Api\Messaging\Response\Error;

class InternalServerError extends Error
{
    public function __construct(string $detail, string $id = null, string $code = null, string $title = 'Internal Server Error')
    {
        parent::__construct($id, "500", $code, $title, $detail);
    }
}