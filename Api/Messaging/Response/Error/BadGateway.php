<?php
namespace Iss\Api\Messaging\Response\Error;

use Iss\Api\Messaging\Response\Error;

class BadGateway extends Error
{
    public function __construct(string $detail, string $id = null, string $code = null, string $title = "Bad Gateway")
    {
        parent::__construct($id, "502", $code, $title, $detail);
    }
}