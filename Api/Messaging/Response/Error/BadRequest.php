<?php
namespace Iss\Api\Messaging\Response\Error;

use Iss\Api\Messaging\Response\Error;

class BadRequest extends Error
{
    public function __construct(string $detail, string $id = null, string $code = null, string $title = "Bad Request")
    {
        parent::__construct($id, "400", $code, $title, $detail);
    }
}