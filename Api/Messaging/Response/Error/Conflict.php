<?php
namespace Iss\Api\Messaging\Response\Error;

use Iss\Api\Messaging\Response\Error;

class Conflict extends Error
{
    public function __construct(string $detail, string $id = null, string $code = null, string $title = "Conflict")
    {
        parent::__construct($id, "409", $code, $title, $detail);
    }
}