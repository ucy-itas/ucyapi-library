<?php
namespace Iss\Api\Messaging\Response\Error;

use Iss\Api\Messaging\Response\Error;

class MethodNotAllowed extends Error
{
    public function __construct(string $detail, string $id = null, string $code = null, string $title = "Method Not Allowed")
    {
        parent::__construct($id, "405", $code, $title, $detail);
    }
}