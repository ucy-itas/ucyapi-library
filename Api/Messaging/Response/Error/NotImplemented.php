<?php
namespace Iss\Api\Messaging\Response\Error;

use Iss\Api\Messaging\Response\Error;

class NotImplemented extends Error
{
    public function __construct(string $detail, string $id = null, string $code = null, string $title = "Not Implemented")
    {
        parent::__construct($id, "501", $code, $title, $detail);
    }
}