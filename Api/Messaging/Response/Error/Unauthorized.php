<?php
namespace Iss\Api\Messaging\Response\Error;

use Iss\Api\Messaging\Response\Error;

class Unauthorized extends Error
{
    public function __construct(string $detail, string $id = null, string $code = null, string $title = "Unauthorized")
    {
        parent::__construct($id, "401", $code, $title, $detail);
    }
}