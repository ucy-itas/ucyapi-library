<?php

namespace Iss\Api\Messaging;

use Iss\Api\Messaging\Request\{Attribute, Condition, Operator, Sort};
use Phalcon\Http\Request as HttpRequest;

class Request
{
    protected string $_method = '';

    protected string $_return_type = '';

    protected string $resource_name = '';

    /**
     * @var Condition[]
     */
    protected array $conditions = [];

    protected \stdClass $data;

    protected $raw_data;

    protected array $_attributes = [];

    protected array $_sort = [];

    protected array $_expansions = [];

    public function __construct(array $conditions = [])
    {
        $data = new \stdClass();
        $data->data = new \stdClass();
        $data->data->type = '';
        $data->data->attributes = new \stdClass();
        $this->setData($data);
        foreach ($conditions as $key => $value) {
            if ($value instanceof Condition) {
                $this->addCondition($value);
            } else {
                $this->__set($key, $value);
            }
        }
    }

    public static function getFromHttpRequest(HttpRequest $request): Request
    {
        /**
         * TODO filtering conditions like this..
         * /apis/classified/server/logs?path=/apis/classfied
         * /apis/classified/server/logs?path.regex=*classfied*
         * /apis/classified/server/logs?method.ne=GET
         * /apis/classified/server/logs?method=POST&method=PATCH&method=PUT <- not this use POST,PATCH,PUT
         * /apis/classified/server/logs?usingTime.gte=1500&usingTime.lte=2500
         * /apis/classified/server/logs?headers.user-agent.regex=*chrome*
         */
        $parameters_array = (function () {
            // php replaces . with _ in variable names so we need to parse the query string
            $pairs = explode("&", $_SERVER['QUERY_STRING']);
            $vars = [];
            foreach ($pairs as $pair) {
                $nv = explode("=", $pair);
                if ($nv) {
                    $name = strtolower(trim(urldecode($nv[0])));
                    if ($name) {
                        $value = urldecode($nv[1] ?? '');
                        if (isset($vars[$name])) {
                            if (!is_array($vars[$name])) {
                                $vars[$name] = [$vars[$name]];
                            }
                            $vars[$name][] = $value;
                        } else {
                            $vars[$name] = $value;
                        }
                    }
                }
            }
            return $vars;
        })();
        $parameters_array["data"] = $request->getRawBody();

        $request_result = new Request();

        $request_result->setMethod($request->getMethod());

        $expansion_parameters = $parameters_array["expand"] ?? '';
        if (is_array($expansion_parameters)) {
            $expansion_parameters = implode(',', $expansion_parameters);
        }
        $expansion_parameters = array_unique(explode(',', strtolower($expansion_parameters)));

        foreach ($expansion_parameters as $expansion_parameter) {
            if($expansion_parameter) {
                $current_request = $request_result;
                $expansion_parameter_array = explode('.', $expansion_parameter);
                for ($i = 0; $i < count($expansion_parameter_array); $i++) {
                    if ($expansion_parameter_array[$i]) {
                        $expansion_exists = $current_request->getExpansion($expansion_parameter_array[$i]);
                        if ($expansion_exists) {
                            $current_request = $expansion_exists;
                        } else {
                            $tmp_request = new Request();
                            $current_request->addExpansion($expansion_parameter_array[$i], $tmp_request);
                            $current_request = $tmp_request;
                        }
                    }
                }
            }
        }

        $attributes = $parameters_array["fields"] ?? '';
        if (is_array($attributes)) {
            $attributes = implode(',', $attributes);
        }
        $attributes = array_unique(explode(',', strtolower($attributes)));
        foreach ($attributes as $key => $attribute) {
            if ($attribute) {
                $attribute_parts = explode('.', $attribute);
                $attribute_name = end($attribute_parts);
                $current_request = $request_result;
                for ($i = 0; $i < count($attribute_parts) - 1; $i++){
                    $current_request = $current_request->getExpansion($attribute_parts[$i]);
                    if (is_null($current_request)) {
                        // a part of the attributes is not defined in expansions so just ignore it
                        continue 2;
                    }
                }
                $current_request->addAttribute(new Attribute($attribute_name));
            }
        }
        $request_result->raw_data = $parameters_array["data"] ?? null;

        $sort_options = $parameters_array["sort"] ?? '';
        if (is_array($sort_options)) {
            $sort_options = implode(',', $sort_options);
        }
        $sort_options = explode(',', strtolower($sort_options));
        foreach ($sort_options as $sort_option) {
            $sort_option = trim($sort_option);
            if ($sort_option) {
                $prefix = substr($sort_option, 0, 1);
                $order = null;
                if ($prefix === "-") {
                    $order = Sort::DESCENDING;
                    $variable = trim(substr($sort_option, 1));
                } elseif ($prefix === "+") {
                    $order = Sort::ASCENDING;
                    $variable = trim(substr($sort_option, 1));
                } else {
                    $order = Sort::ASCENDING;
                    $variable = $sort_option;
                }
                $sort_parts = explode('.', $variable);
                $variable_name = end($sort_parts);
                $current_request = $request_result;
                for ($i = 0; $i < count($sort_parts) - 1; $i++){
                    $current_request = $current_request->getExpansion($sort_parts[$i]);
                    if (is_null($current_request)) {
                        // a part of the sort order is not defined in expansions so just ignore it
                        continue 2;
                    }
                }
                $current_request->addSort(new Sort($variable_name, $order));
            }
        }
        unset(
            $parameters_array['fields'],
            $parameters_array['_url'],
            $parameters_array['data'],
            $parameters_array['sort'],
            $parameters_array['expand']
        );

        foreach ($parameters_array as $variable => $value) {
            $variable_parts = explode('.', $variable);
            $operator = Operator::tryFrom(end($variable_parts));
            if (is_null($operator)) {
                $operator = Operator::IN;
                $variable_parts[] = $operator->value;
            }
            if(count($variable_parts) >= 2) {
                $current_request = $request_result;
                for ($i = 0; $i < count($variable_parts) - 2; $i++) {
                    $current_request = $current_request->getExpansion($variable_parts[$i]);
                    if (is_null($current_request)) {
                        // a part of the path is not defined in expansions so just ignore it
                        continue 2;
                    }
                }
                $variable = $variable_parts[$i];
                if (!is_array($value)) {
                    $value = [$value];
                }
                foreach ($value as $item) {
                    $item = array_unique(explode(',', $item));
                    $current_request->addCondition(new Condition($variable, $item, $operator));
                }
            }
        }
        return $request_result;
    }

    public function getConditions(array|string|null $name = null, array|string|null $target = null): array
    {
        if (!is_null($name) || !is_null($target)) {
            if (!is_null($name)) {
                $result = [];
                $conditions = $this->conditions;
                $names = $name;
                if (!is_array($names)) {
                    $names = [$names];
                }
                foreach ($names as $name) {
                    foreach ($conditions as $key => $condition) {
                        if ($condition->getVariable()->getName() === $name) {
                            $result[] = $condition;
                        }
                    }
                }
                return array_unique($result);
            }
            if (!is_null($target)) {
                $result = [];
                $conditions = $this->conditions;
                $targets = $target;
                if (!is_array($targets)) {
                    $targets = [$targets];
                }
                foreach ($targets as $target) {
                    foreach ($conditions as $key => $condition) {
                        if (in_array($target, $condition->getVariable()->getTarget())) {
                            $result[] = $condition;
                        }
                    }
                }
                return array_unique($result, SORT_REGULAR);
            }
        }
        return $this->conditions;
    }

    public function setConditions(array $conditions): Request
    {
        $this->conditions = [];
        foreach ($conditions as $condition) {
            $this->addCondition($condition);
        }
        return $this;
    }

    public function addCondition(Condition $condition): Request
    {
        $this->conditions[] = $condition;
        return $this;
    }

    public function hasConditionsOn(string $name = '', string $target = ''): bool
    {
        $i = 0;
        $conditions = $this->getConditions();
        if ($name) {
            foreach ($conditions as $key => $condition) {
                if ($condition->getVariable()->getName() === $name) {
                    return true;
                }
            }
            return false;
        }
        if ($target) {
            foreach ($conditions as $key => $condition) {
                if (in_array($target, $condition->getVariable()->getTarget())) {
                    return true;
                }
            }
            return false;
        }
        return (count($conditions) > 0);
    }

    //TODO change Data to Body
    public function getData(): \stdClass
    {
        return $this->data;
    }

    public function setData(\stdClass $data): Request
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRawData()
    {
        return $this->raw_data;
    }



    public function getAttributes(): array
    {
        return $this->_attributes;
    }

    public function getAttribute(string $name): ?Attribute
    {
        return ($this->_attributes[$name] ?? null);
    }

    public function setAttributes(array $attributes): Request
    {
        $this->_attributes = [];
        foreach ($attributes as $attribute) {
            if ($attribute instanceof Attribute) {
                $this->addAttribute($attribute);
            } elseif (is_string($attribute)) {
                $this->addAttribute(new Attribute($attribute));
            }
        }
        return $this;
    }

    public function addAttribute(Attribute $attribute): Request
    {
        $this->_attributes[$attribute->getName()] = $attribute;
        return $this;
    }

    public function hasAttribute(string $name): bool
    {
        return isset($this->_attributes[$name]);
    }

    public function deleteAttribute(string $name): Request
    {
        unset($this->_attributes[$name]);
        return $this;
    }

    public function getSort() : array
    {
        return $this->_sort;
    }

    public function setSort(array $options): Request
    {
        $this->_sort = [];
        foreach ($options as $option) {
            if ($option instanceof Sort) {
                $this->addSort($option);
            }
        }
        return $this;
    }

    public function addSort(Sort $option): Request
    {
        $this->_sort[$option->getVariable()->getName()] = $option;
        return $this;
    }

    public function __set($name, $value)
    {
        if ($name === 'return') {
            if (!is_array($value)) {
                $value = [$value];
            }
            $this->setAttributes($value);
        } else {
            $this->addCondition(new Condition($name, $value));
        }
    }

    public function getMethod(): string
    {
        return $this->_method;
    }

    public function setMethod(string $method): Request
    {
        $this->_method = $method;
        if (!$this->getReturnType()) {
            $this->setReturnType('resource');
        }
        return $this;
    }

    public function getReturnType(): string
    {
        return $this->_return_type;
    }

    public function setReturnType(string $return_type): Request
    {
        $this->_return_type = $return_type;
        return $this;
    }

    public function getResourceName(): string
    {
        return $this->resource_name;
    }

    public function setResourceName(string $resource_name): Request
    {
        $this->resource_name = $resource_name;
        return $this;
    }

    public function getExpansions(): array
    {
        return $this->_expansions;
    }

    public function getExpansion(string $expansion): ?Request
    {
        return $this->_expansions[$expansion] ?? null;
    }

    public function setExpansions(array $expansions): void
    {
        $this->_expansions = $expansions;
    }

    public function addExpansion(string $name, Request $expansion): void
    {
        $this->_expansions[$name] = $expansion;
    }

    public function toUrlParameters(): array
    {
        $result = [
            'fields' => [],
            'expand' => [],
            'sort' => []
        ];
        $conditions = $this->getConditions();
        /**
         * @var $condition Condition
         */
        foreach ($conditions as $condition) {
            $parameter = $condition->getVariable()->getName() . '.' . $condition->getOperator()->value;
            $result[$parameter] = $condition->getValues();
        }

        $attributes = $this->getAttributes();
        /**
         * @var $attribute Attribute
         */
        foreach ($attributes as $attribute) {
            $result['fields'][] = $attribute->getName();
        }

        $sort_options = $this->getSort();
        /**
         * @var $sort_option Sort
         */
        foreach ($sort_options as $sort_option) {
            $prefix = '+';
            if ($sort_option->getOrder() == Sort::DESCENDING) {
                $prefix = '-';
            }
            $result['sort'][] = $prefix . $sort_option->getVariable()->getName();
        }

        $expansions = $this->getExpansions();
        $result['expand'] = array_keys($expansions);

        /**
         * @var $expansion_request Request
         */
        foreach ($expansions as $expansion_name => $expansion_request) {
            $expansion_request_parameters = $expansion_request->toUrlParameters();
            foreach ($expansion_request_parameters['sort'] as $sub_sort) {
                $result['sort'][] = substr($sub_sort, 0, 1) . $expansion_name . '.' . substr($sub_sort, 1);
            }
            foreach ($expansion_request_parameters['fields'] as $sub_field) {
                $result['fields'][] = $expansion_name . '.' . $sub_field;
            }
            foreach ($expansion_request_parameters['expand'] as $sub_expand) {
                $result['expand'][] = $expansion_name . '.' . $sub_expand;
            }
            unset($expansion_request_parameters['sort'], $expansion_request_parameters['fields'], $expansion_request_parameters['expand']);
            foreach ($expansion_request_parameters as $variable_name => $variable_value) {
                $result[$expansion_name . '.' . $variable_name] = $variable_value;
            }
        }

        return $result;
    }

}
