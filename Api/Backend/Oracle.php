<?php
namespace Iss\Api\Backend;

use Iss\Api\Messaging\Request;
use Phalcon\Config\Config;
use Iss\Api\Backend\Db\Pdo;
use Iss\Api\Messaging\Request\Condition;

class Oracle implements BackendInterface
{
    protected Pdo $_pdo;

    protected ?string $_select = null;

    protected ?string $_from = null;

    protected ?string $_where = null;

    protected ?array $_values = [];

    protected ?string $order_by = null;

    protected ?string $group_by = null;

    protected ?string $limit = null;

    protected ?string $offset = null;

    protected $_return_iterator;

    protected $_fetch_method;

    protected Config $config;

    public function __construct(Config $config)
    {
        $this->config = $config;
        $this->reset();
    }

    protected function reset(): void
    {
        $config = $this->getConfig();
        $this->select('')
            ->from($config->path('options.from', ''))
            ->where($config->path('options.where', ''), false)
            ->groupBy($config->path('options.group_by', ''), false)
            ->orderBy($config->path('options.order_by', ''), false);
        $this->_return_iterator = ($config->path('options.return', '') === 'iterator');
        $this->_fetch_method = $config->path('options.fetch_method', \PDO::FETCH_ASSOC);
    }

    public function getPdo(): Pdo
    {
        if (!isset($this->_pdo)) {
            $this->_pdo = new Pdo($this->getConfig()->get('db', $this->getConfig()));
        }
        return $this->_pdo;
    }

    protected function getQuery(): string
    {
        $query = "";
        if ($this->_select) {
            $query .= "select $this->_select from $this->_from ";
            $query .= ($this->_where) ? "where $this->_where " : "";
            $query .= ($this->group_by) ? "group by $this->group_by " : "";
            $query .= ($this->order_by) ? "order by $this->order_by " : "";
        }

        if ($query && !is_null($this->limit)) {
            $query = $query . " offset " . ($this->offset ?: 0) . " rows fetch next $this->limit rows only ";
        }
        return $query;
    }

    protected function _prepareSelection (array $attributes): string
    {
        $requested_as_attributes = [];
        foreach ($attributes as $attribute) {
            $as_attribute = $attribute->getName();
            $requested_attribute = $attribute->getTarget();
            $requested_as_attributes[] = "$requested_attribute as \"$as_attribute\"";
        }
        return implode(', ', $requested_as_attributes);
    }

    private function _buildRow(Condition $condition): array
    {
        /*
         * Each Condition is in a form of [a,b,c]=d,e,f
         * this translates to "and (a in (d,e,f) or b in (d,e,f) or c in (d,e,f))"
         * or actually to and (a in (?,?,?) or b in (?,?,?) or c in (?,?,?))
         * with values [d,e,f,d,e,f,d,e,f]
         *
         * Each Condition is in a form of a|b|c=!d,e,f
         * this translates to "and (a not in (d,e,f) and b not in (d,e,f) and c not in (d,e,f))"
         * or actually to and (a not in (?,?,?) and b not in (?,?,?) and c not in (?,?,?))
         * with values [d,e,f,d,e,f,d,e,f]
         *
         * Conditions in the form a=>d,e,f tranlates to a > d,e,f (value is kept as is)
         * */

        $result = ['where' => [], 'values' => $condition->getValues()];
        switch ($condition->getOperator()) {
            case Request\Operator::GREATER_THAN:
                $result['where'] = '(' . $condition->getVariable()->getTarget()[0] . ' > ?)';
                return $result;
            case Request\Operator::GREATER_THAN_OR_EQUAL:
                $result['where'] = '(' . $condition->getVariable()->getTarget()[0] . ' >= ?)';
                return $result;
            case Request\Operator::LESS_THAN:
                $result['where'] = '(' . $condition->getVariable()->getTarget()[0] . ' < ?)';
                return $result;
            case Request\Operator::LESS_THAN_OR_EQUAL:
                $result['where'] = '(' . $condition->getVariable()->getTarget()[0] . ' <= ?)';
                return $result;
            default :
                $result['values'] = [];
                break;
        }

        $search_keyword = "in";
        $join_keyword = "or";
        if (in_array($condition->getOperator(), [Request\Operator::NOT_IN, Request\Operator::NOT_EQUALS])) {
            $search_keyword = "not in";
            $join_keyword = "and";
        }
        if (in_array($condition->getOperator(), [Request\Operator::IN, Request\Operator::EQUALS])) {
            $search_keyword = "in";
            $join_keyword = "or";
        }
        $keys = $condition->getVariable()->getTarget();
        $values = $condition->getValues();
        $in = str_repeat('?,', count($values) - 1) . '?';

        foreach ($keys as $index => $key) {
            $key = str_replace('+', '||', $key);
            $keys[$index] = "$key $search_keyword ($in)";
            $result['values'] = array_merge($result['values'], $values);
        }
        $result['where'][] = "(" . implode(" $join_keyword ", $keys) . ")";

        $result['where'] = implode(' and ', $result['where']);
        return $result;
    }

    protected function _prepareConditions (array $conditions): array
    {
        /*
         * Each Condition is in a form of a|b|c=d,e,f
         * this translates to "and (a in (d,e,f) or b in (d,e,f) or c in (d,e,f))"
         * or actually to and (a in (?,?,?) or b in (?,?,?) or c in (?,?,?))
         * with values [d,e,f,d,e,f,d,e,f]
         *
         * Each Condition is in a form of a|b|c=!d,e,f
         * this translates to "and (a not in (d,e,f) and b not in (d,e,f) and c not in (d,e,f))"
         * or actually to and (a not in (?,?,?) and b not in (?,?,?) and c not in (?,?,?))
         * with values [d,e,f,d,e,f,d,e,f]
         * */

        $result = ['where' => [], 'values' => []];
        foreach ($conditions as $key => $condition) {
            $row_result = $this->_buildRow($condition);
            $result['where'][] = $row_result['where'];
            $result['values'] = array_merge($result['values'], $row_result['values']);
        }
        $result['where'] = implode(' and ', $result['where']);
        return $result;
    }

    protected function _addConditions(array $conditions): string
    {
        $conditions = $this->_prepareConditions($conditions);

        $this->_where .= " ${conditions['where']} ";
        $this->_values = $conditions['values'];
        return $this->_where;
    }

    protected function select($select): BackendInterface
    {
        if (is_array($select)) {
            $select = $this->_prepareSelection($select);
        }
        $this->_select = $select;
        return $this;
    }

    protected function from(string $from): BackendInterface
    {
        $this->_from = $from;
        return $this;
    }

    protected function where($where, bool $append = true): BackendInterface
    {
        $append = ($this->_where) ? $append : false;
        $extra_conditions = ['where' => '', 'values' => []];
        if (is_array($where)) {
            foreach ($where as $index => $condition) {
                if ($condition->getVariable()->getName() === 'limit') {
                    $this->limit = $condition->getValue();
                    unset($where[$index]);
                }
                if ($condition->getVariable()->getName() === 'offset') {
                    $this->offset = $condition->getValue();
                    unset($where[$index]);
                }
            }
            $extra_conditions = $this->_prepareConditions($where);
        } else {
            $extra_conditions['where'] = $where;
        }

        if ($append) {
            if ($extra_conditions['where']) {
                $this->_where .= " and ${extra_conditions['where']} ";
                $this->_values = array_merge($this->_values, $extra_conditions['values']);
            }
        } else {
            $this->_where = $extra_conditions['where'];
            $this->_values = $extra_conditions['values'];
        }
        return $this;
    }

    protected function orderBy(string $order_by, bool $append = true): BackendInterface
    {
        $append = ($this->order_by) ? $append : false;

        if ($append) {
            if ($order_by) {
                $this->order_by .= ", $order_by";
            }
        } else {
            $this->order_by = $order_by;
        }
        return $this;
    }

    protected function groupBy(string $group_by, bool $append = true): BackendInterface
    {
        $append = ($this->group_by) ? $append : false;

        if ($append) {
            if ($group_by) {
                $this->group_by .= ",$group_by";
            }
        } else {
            $this->group_by = $group_by;
        }
        return $this;
    }

    public function get(Request $request)
    {
        $conditions = $request->getConditions();
        $this->select($request->getAttributes())->where($conditions);
        if ($this->_return_iterator) {
            $statement = $this->getPdo()->prepare($this->getQuery(), [\PDO::ATTR_CURSOR => \PDO::CURSOR_SCROLL]);
            $result = $statement->execute($this->_values);
            if (!$result) {
                var_dump($statement->errorInfo());
                exit(0);
            }
            $statement->setFetchMode($this->_fetch_method);
            $this->reset();
            return new \IteratorIterator($statement);
        } else {
            $statement = $this->getPdo()->prepare($this->getQuery());
            $result = $statement->execute($this->_values);
            if (!$result) {
                var_dump($statement->errorInfo());
                exit(0);
            }
            $this->reset();
            return $statement->fetchAll($this->_fetch_method);
        }
    }

    public function put(Request $request)
    {
        throw new NotImplementedException();
    }

    public function patch(Request $request)
    {
        throw new NotImplementedException();
    }

    public function post(Request $request)
    {
        $data = $request->getData()->data->attributes;
        $insert_properties = [];
        $insert_values = [];
        foreach ($data as $property => $value) {
            $insert_properties[] = $property;
            $insert_values[] = $value;
        }
        $sql = "insert into ".$this->_from." (".implode(', ', $insert_properties).") values (".str_repeat('?,', count($insert_values) - 1) . '?'.")";
        $statement = $this->getPdo()->prepare($sql);
        $result = $statement->execute($insert_values);
        return $result;
    }

    public function delete(Request $request)
    {
        $this->where($request->getConditions());
        $sql = "delete from " . $this->_from . " where " . $this->_where;
        $statement = $this->getPdo()->prepare($sql);
        $result = $statement->execute($this->_values);
        return $result;
    }

    public function supportsRemapping(): bool
    {
        return true;
    }

    public function getConfig(): Config
    {
        return $this->config;
    }
}
