<?php
namespace Iss\Api\Backend;

use Iss\Api\Messaging\Request;

interface BackendInterface
{
    public function get(Request $request);

    public function put(Request $request);

    public function post(Request $request);

    public function patch(Request $request);

    public function delete(Request $request);

    public function supportsRemapping(): bool;
}