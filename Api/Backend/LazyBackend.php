<?php
namespace Iss\Api\Backend;

use Iss\Api\Messaging\Request;
use Phalcon\Config\Config;

class LazyBackend implements BackendInterface
{
    protected ?BackendInterface $_backend = null;

    protected string $_definition;

    protected Config $_config;

    public function __construct(string $definition, Config $config)
    {
        $this->_definition = $definition;
        $this->_config = $config;
    }

    public function get(Request $request)
    {
        if (!is_object($this->_backend)) {
            $this->_backend = new $this->_definition($this->getConfig());
        }
        return $this->_backend->get($request);
    }

    public function post(Request $request)
    {
        if (!is_object($this->_backend)) {
            $this->_backend = new $this->_definition($this->getConfig());
        }

        return $this->_backend->post($request);
    }

    public function put(Request $request)
    {
        if (!is_object($this->_backend)) {
            $this->_backend = new $this->_definition($this->getConfig());
        }

        return $this->_backend->put($request);
    }

    public function patch(Request $request)
    {
        if (!is_object($this->_backend)) {
            $this->_backend = new $this->_definition($this->getConfig());
        }

        return $this->_backend->patch($request);
    }

    public function delete(Request $request)
    {
        if (!is_object($this->_backend)) {
            $this->_backend = new $this->_definition($this->getConfig());
        }

        return $this->_backend->delete($request);
    }

    public function supportsRemapping(): bool
    {
        if (!is_object($this->_backend)) {
            $this->_backend = new $this->_definition($this->getConfig());
        }

        return $this->_backend->supportsRemapping();
    }

    public function setConfig(Config $config)
    {
        $this->_config = $config;
        if (is_object($this->_backend)) {
            $this->_backend->setConfig($config);
        }
    }

    public function getConfig(): Config
    {
        return $this->_config;
    }
}