<?php
namespace Iss\Api\Backend;

use Iss\Api\Messaging\Request;
use Iss\Api\Messaging\Request\Condition;
use Phalcon\Config\Config;
use Ucy\Iss\Component\Ldap\Core\Manager;
use Ucy\Iss\Component\Ldap\Platform\Native\Driver;
use Iss\Api\Exception\NotImplementedException;

class Ldap implements BackendInterface
{
    private Manager $_manager;

    private Config $_config;

    function __construct(Config $config)
    {
        $this->_config = $config;

        $params = array(
            'hostname' => $config->hostname,
            'port' => $config->port,
            'base_dn' => $config->base_dn,
            //'options' => array(LDAP_OPT_SIZELIMIT => 1000)
        );

        $this->_manager = new Manager($params);
    }

    public function get(Request $request): array
    {
        /* Build search Condition */
        $search_condition = $this->_prepareConditions($request);

        $cleaned_properties = [];
        $attributes = $request->getAttributes();
        foreach ($attributes as $attribute) {
            $cleaned_properties[] = $attribute->getTarget();
        }

        if ($search_condition) {
            $this->_manager->connect();
            $this->_manager->bind($this->_config->principal, $this->_config->password);

            $search_results = $this->_manager->search(null, $search_condition, true, $cleaned_properties);
            $result = [];
        } else {
            $search_results = [];
            $result = [];
        }

        foreach ($search_results as $node) {
            $cleaned_results = [];
            if (in_array('dn', $cleaned_properties)) {
                $key = 'dn';
                $cleaned_results[$key] = [$node->getDn()];
            }

            $returned_attributes = $node->getAttributes();

            foreach ($returned_attributes as $key => $returned_attribute) {
                $nodeAttr = $node->get($key);
                $cleaned_results[$key] = (is_null($nodeAttr)) ? null : $nodeAttr->getValues();
            }
            $result[] = $cleaned_results;
        }

        return $result;
    }

    public function put(Request $request)
    {
        throw new NotImplementedException();
    }

    public function post(Request $request)
    {
        throw new NotImplementedException();
    }

    public function patch(Request $request)
    {
        throw new NotImplementedException();
    }

    public function delete(Request $request)
    {
        throw new NotImplementedException();
    }


    public function supportsRemapping(): bool
    {
        return false;
    }

    private function _buildRow(Condition $condition): string
    {
        /*
         * Each Condition is in a form of a|b|c=d,e,f
         * this translates to "(|(a=d)(a=e)(a=f)(b=d)(b=e)...)"
         *
         * Each Condition is in a form of a|b|c=!d,e,f
         * this translates to "(&(!(a=d))(!(a=e))(!(a=f))(!(b=d))(!(b=e))...)"
         * */

        $search_condition = "";

        if (in_array($condition->getOperator(), [Request\Operator::NOT_IN, Request\Operator::NOT_EQUALS])) {
            $search_keyword = "!";
            $join_keyword = "&";
        } else {
            $search_keyword = "";
            $join_keyword = "|";
        }

        $keys = $condition->getVariable()->getTarget();
        $values = $condition->getValues();

        foreach ($keys as $ki => $key) {
            foreach ($values as $vi => $value_data) {
                if ($search_keyword) {
                    $search_condition .= "($search_keyword($key=$value_data))";
                } else {
                    $search_condition .= "($key=$value_data)";
                }
            }
        }
        if ((count($keys) * count($values)) > 1) {
            $search_condition = "($join_keyword$search_condition)";
        }
        return $search_condition;
    }

    protected function _prepareConditions(Request $request): string
    {
        /* Build search Condition */
        $search_condition = "";
        $conditions = $request->getConditions();

        foreach ($conditions as $key => $condition) {
            $search_condition .= $this->_buildRow($condition);
            if (count($condition->getValues()) > 1) {
                $search_condition = "(&$search_condition)";
            }
        }
        if (count($conditions) > 1) {
            $search_condition = "(&$search_condition)";
        }
        return $search_condition;
    }
}
