<?php

namespace Iss\Api\Backend\Db;

use Phalcon\Config\Config;

class Pdo extends \PDO
{
    public function __construct(Config $config)
    {
        // Copy the config because it uses unset.
        $config = new Config($config->toArray());
        $username = $config->get('username');
        $password = $config->get('password');
        $type = $config->get('type', 'oci');
        unset($config->username, $config->password, $config->type);

        $options = $config->get('options', new Config())->toArray();
        if (isset($options['persistent'])) {
            $options[PDO::ATTR_PERSISTENT] = $options['persistent'];
            unset($options['persistent']);
        }
        unset($config->options);

        $dsnParts = [];
        foreach ($config->toArray() as $key => $value) {
            $dsnParts[] = $key . "=" . $value;
            unset($config->$key);
        }

        $dsnAttributes = implode(";", $dsnParts);
        parent::__construct("$type:$dsnAttributes", $username, $password, $options);
    }
}
