<?php
namespace Iss\Api\Backend;

use Iss\Api\Messaging\Request;

class NullBackend implements BackendInterface
{
    protected Config $_config;

    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    public function get(Request $request)
    {
        return [];
    }

    public function put(Request $request)
    {
        return [];
    }

    public function post(Request $request)
    {
        return [];
    }

    public function patch(Request $request)
    {
        return [];
    }

    public function delete(Request $request)
    {
        return [];
    }

    public function supportsRemapping(): bool
    {
        return false;
    }
}