<?php
namespace Iss\Api;

trait ServiceTrait
{
    private int $priority = 100;

    public function getPriority(): int
    {
        return $this->priority;
    }

    public function setPriority(int $priority): ServiceInterface
    {
        $this->priority = $priority;
        return $this;
    }

    public function setPriorityBefore(ServiceInterface $service): ServiceInterface
    {
        if ($this->getPriority() <= $service->getPriority()) {
            return $this->setPriority($service->getPriority() + 1);
        }
        return $this;
    }

    public function setPriorityAfter(ServiceInterface $service): ServiceInterface
    {
        if ($this->getPriority() >= $service->getPriority()) {
            return $this->setPriority($service->getPriority() - 1);
        }
        return $this;
    }
}