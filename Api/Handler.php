<?php
namespace Iss\Api;

use Iss\Api\Backend\{BackendInterface, Db\Pdo, LazyBackend};
use Iss\Api\Exception\ApiException;
use Iss\Api\Messaging\Request;
use Iss\Api\Messaging\Request\Attribute;
use Iss\Api\Messaging\Response\Error\NotImplemented;
use Iss\Api\Messaging\Response\Error;
use Phalcon\{Config\Config, Events\Manager};
use Psr\Log\{LoggerAwareInterface, LoggerAwareTrait, LoggerInterface, NullLogger};
use Swaggest\JsonSchema\Schema;

class Handler implements HandlerInterface, LoggerAwareInterface
{
    use LoggerAwareTrait;

    /**
     * Allowed parameters
     * @var Request\Variable[] $parameters
     */
    protected readonly array $parameters;
    /**
     * All properties
     */
    protected readonly array $properties;
    protected readonly array $property_groups;

    /**
     * Parameters that are filtered by the handler
     */
    protected readonly array $filter_parameters;
    protected readonly string $name;

    /**
     * parameters for filtering on the handler
     */
    protected array $local_parameters = [];

    protected BackendInterface $backend;

    protected ?Manager $events_manager = null;

    protected Config $config;

    protected Request $request;

    public function __construct(Config $config, ?LoggerInterface $logger = null)
    {
        $this->setLogger(is_null($logger) ? new NullLogger() : $logger);
        $this->configure($config);
    }

    protected function configure(Config $config)
    {
        $this->clearFilter();
        $empty_config = new Config();
        $this->setConfig($config);
        /**
         * @var $handler_config Config
         */
        $handler_config = $config->get('handler', $empty_config);
        $this->name = $handler_config->get('name', '');

        $parameters = array_change_key_case($handler_config->get('parameters', $empty_config)->toArray(), CASE_LOWER);
        $allowed_parameters = [];
        foreach ($parameters as $name => $parameter) {
            $variable = new Request\Variable(name: $name, target: $parameter['target'] ?? null);
            if (isset($parameter['schema'])) {
                $schema = (object) $parameter['schema'];
                $variable->setSchema(Schema::import($schema));
            }
            $allowed_parameters[$name] = $variable;
        }
        $this->parameters = $allowed_parameters;

        $properties = array_change_key_case($handler_config->get('properties', $empty_config)->toArray(), CASE_LOWER);
        foreach ($properties as $key => $property) {
            if (!isset($property['target']) || is_null($property['target'])) {
                $properties[$key]['target'] = $key;
            }
            if (!isset($property['type']) || is_null($property['type'])) {
                $properties[$key]['type'] = 'string';
            }
        }
        $this->properties = $properties;

        $property_groups = [
            'always' => $handler_config->path('property_groups.always', $empty_config)->toArray(),
            'default' => $handler_config->path('property_groups.default', $empty_config)->toArray(),
            'optional' => $handler_config->path('property_groups.optional', $empty_config)->toArray()
        ];
        $properties = $handler_config->get("properties", $empty_config)->toArray();
        foreach ($properties as $property_name => $property) {
            $properties[$property_name] = new Request\Attribute(
                $property_name,
                $properties[$property_name]['type'] ?? null,
                $properties[$property_name]['target'] ?? null,
                $properties[$property_name]['callback'] ?? null
            );
        }
        foreach ( $property_groups as $group => $group_members) {
            foreach ($group_members as $key => $property_name) {
                if (isset($properties[$property_name])) {
                    $property_groups[$group][$key] = $properties[$property_name];
                    unset($properties[$property_name]);
                } else {
                    unset( $property_groups[$group][$key]);
                }
            }
        }
        foreach ($properties as $property_name => $property) {
            $property_groups['default'][] = $properties[$property_name];
        }

        $this->property_groups = $property_groups;
        $this->filter_parameters = $handler_config->get('filter', new Config())->toArray();

        $adapter_class_name = $handler_config->path('adapter.class', null);
        if ($adapter_class_name) {
            if ($handler_config->path('adapter.lazy', false)) {
                $this->setBackend(new LazyBackend($adapter_class_name, $this->getConfig()));
            } else {
                $this->setBackend(new $adapter_class_name($this->getConfig()));
            }
        } else {
            $backend_class_name = $handler_config->path('backend.class');
            if ($backend_class_name) {
                if ($handler_config->path('backend.lazy', false)) {
                    $this->setBackend(
                        new LazyBackend(
                            $backend_class_name,
                            $this->getConfig()->get('backend', $empty_config)
                        )
                    );
                } else {
                    $this->setBackend(
                        new $backend_class_name($this->getConfig()->get('backend', $empty_config))
                    );
                }
            }
        }
    }

    public function handle(Request $request)
    {
        $this->setRequest($request);
        $request = $this->getRequest();

        switch ($request->getMethod()) {
            case 'POST':
                return $this->post($request);
            case 'PATCH':
                return $this->patch($request);
            case 'PUT':
                return $this->put($request);
            case 'DELETE':
                return $this->delete($request);
            case 'GET':
            case 'HEAD':
                if ($request->getReturnType() === 'resource') {
                    return $this->getResource($request);
                } else {
                    return $this->getCollection($request);
                }
            default :
                return new NotImplemented("Method not implemented");
        }
    }

    protected function _first($input)
    {
        if ($input instanceof \Iterator) {
            $input->rewind();
            return $input->current() ?? false;
        }
        if (is_array($input)) {
            if (isset($input[0])) {
                if (count($input) > 1) {
                    $logger = $this->logger;
                    $context = [
                        'resource_name' => $this->name,
                        'result_count' => count($input)
                    ];
                    $logger->warning('[%resource_name%] more resources retrieved, expected one:[%result_count%]', $context);
                }
                return $input[0];
            } elseif (count($input) > 0) {
                return $input;
            } else {
                return false;
            }
        }
        return $input;
    }

    protected function _getResources(Request $request)
    {
        $request = $this->prepareRequest($request);

        $this->setRequest($request);
        $request = $this->getRequest();
        if ($this->getEventsManager()) {
            $this->getEventsManager()->fire('handler:beforeExecuteBackend', $this);
        }

        $result = $this->getBackend()->get($request);
        if ($result instanceof Error or $result instanceof \Iterator) {
            return $result;
        }

        if (is_array($result)) {
            // if it is an array of objects convert the objects into associative arrays
            $result = $this->to_array($result);
            $result = $this->_remap($result);
            $result = $this->_filter($result);
        }
        return $result;
    }

    protected function prepareRequest(Request $request): Request
    {
        $this->setParameters($request)->setAttributes($request)->setSort($request);

        return $request;
    }

    protected function setParameters(Request $request): Handler
    {
        $conditions = $request->getConditions();
        $request->setResourceName($this->name);
        $context = ['resource_name' => $this->name];
        /**
         * Add conditions that are not requested but have a default value
         */
        foreach ($this->parameters as $name => $variable) {
            if ($variable->hasSchema()) {
                $default = $variable->getSchema()->getDefault();
                if (!is_null($default) and !$request->hasConditionsOn(name: $name)) {
                    $conditions[] = new Request\Condition($variable, $default);
                }
            }
        }

        /**
         * @var Request\Condition $condition
         */
        foreach ($conditions as $key => $condition) {
            /**
             * Check if requested conditions are allowed and set targets
             */
            $variable_name = $condition->getVariable()->getName();

            if (array_key_exists($variable_name, $this->parameters)) {
                $conditions[$key]->setVariable($this->parameters[$variable_name]);
                //$conditions[$key]->getVariable()->setTarget($this->parameters[$variable_name]->getTarget());
            } else {
                $context['variable'] = $variable_name;
                $this->logger->warning("[%resource_name%] Removed variable [%variable%] from request, variable is not allowed", $context);
                unset($conditions[$key]);
            }
        }

        /**
         * If there are conditions that are filtered later remove them from request
         */
        foreach ($conditions as $key => $condition) {
            $variable_target = $condition->getVariable()->getTarget();

            // TODO: fix this better
            $variable_target = reset($variable_target);

            if (in_array($variable_target, $this->filter_parameters)) {
                $this->addFilter($condition);
                unset($conditions[$key]);
            }
        }

        $request->setConditions($conditions);
        $this->validateConditions($request);
        return $this;
    }

    protected function setAttributes(Request $request): Handler
    {
        if (!$request->getAttributes()) {
            foreach ($this->property_groups['default'] as $default) {
                $request->addAttribute($default);
            }
        }
        foreach ($this->property_groups['always'] as $mandatory) {
            $request->addAttribute($mandatory);
        }

        $attributes = $request->getAttributes();
        $properties = $this->properties;
        foreach ($attributes as $property_key => $requested_property) {
            if (!array_key_exists($requested_property->getName(), $properties)) {
                $context['attribute'] = $requested_property->getName();
                $this->logger->warning("[%resource_name%] Removed attribute [%attribute%] from request, attribute is not allowed", $context);
                unset($attributes[$property_key]);
            } else {
                $properties[$requested_property->getName()]['name'] = $requested_property->getName();
                $attributes[$property_key] = Attribute::fromArray($properties[$requested_property->getName()]);
            }
        }
        $request->setAttributes($attributes);

        return $this;
    }

    protected function setSort(Request $request): Handler
    {
        $sort = $request->getSort();
        if ($sort) {
            $properties = $this->properties;
            foreach ($sort as $property_key => $requested_property) {
                if (!array_key_exists($requested_property->getVariable()->getName(), $properties)) {
                    $context['attribute'] = $requested_property->getVariable()->getName();
                    $this->logger->warning("[%resource_name%] Removed sort attribute [%attribute%] from request, attribute is not allowed", $context);
                    unset($sort[$property_key]);
                } else {
                    $requested_property->getVariable()->setTarget($properties[$requested_property->getVariable()->getName()]['target']);
                    $requested_property->getVariable()->setType($properties[$requested_property->getVariable()->getName()]['type']);
                }
            }
            $request->setSort($sort);
        }

        return $this;
    }

    /**
     * Performs filtering of the $input.
     * The function filters
     *  1. on entire items using the local filter parameters if set
     *  2. on properties using the request return fields
     *
     * @param array $input
     * @return array
     */

    protected function _filter(array $input): array
    {
        $local_filter = $this->getFilter();
        if ($local_filter) {
            $filter = function ($returned_item) use ($local_filter){
                /**
                 * @var Request\Condition $condition
                 */
                foreach ($local_filter as $key => $condition) {
                    $name = $condition->getVariable()->getName();
                    if (isset($returned_item[$name])) {
                        if ($condition->apply($returned_item[$name])) {
                            continue;
                        }
                    }
                    return false;
                }
                return true;
            };

            if (is_array($input)) {
                $array_result = [];
                // Apply extra filtering on other conditions
                foreach ($input as $item) {
                    if ($filter($item) === true) {
                        $array_result[] = $item;
                    }
                }
                $input = $array_result;
            }
        }

        if (is_array($input)) {
            $requested_properties = [];
            $properties = $this->getRequest()->getAttributes();
            foreach ($properties as $property) {
                $requested_properties[$property->getName()] = $property->getName();
            }

            foreach ($input as $key => $item) {
                if (isset($item[0])) {
                    foreach ($item as $item_key => $item_array) {
                        $item[$item_key] = array_intersect_key($item_array, $requested_properties);
                    }
                    $input[$key] = $item;
                } else {
                    $input[$key] = array_intersect_key($item, $requested_properties);
                }
            }
        }

        return $input;
    }

    protected function _path(array $item, string $path, $default = null)
    {
        $path_parts = explode('.', $path);
        $result = $item;
        foreach ($path_parts as $path_part) {
            if (isset($result[$path_part])) {
                $result = $result[$path_part];
            } else {
                return $default;
            }
        }
        return $result;
    }

    protected function _remapNode(array $item, Attribute $attribute)
    {
        $target = $attribute->getTarget();
        switch ($attribute->getType()) {
            case 'array':
                $result = [];
                $sub_attributes = $attribute->getSubAttributes();
                $sub_attribute = reset($sub_attributes);
                if ($sub_attribute->getType() === 'string') {
                    $result = $this->_convert($this->_path($item, $target, []), 'array');
                    $this->_applyCallback($result, $attribute->getCallback());
                } else {
                    $sub_items = [];
                    $target_item = $this->_path($item, $target);

                    if ($target_item) {
                        if (is_array($target_item) && isset($target_item[0])) {
                            $sub_items = $target_item;
                        } else {
                            $sub_items = [$target_item];
                        }
                    }

                    foreach ($sub_items as $sub_item) {
                        $result[] = $this->_remapNode($sub_item, $sub_attribute);
                    }
                }
                break;
            case 'object':
                $result = new \stdClass();
                $sub_node = $this->_path($item, $target, $item);
                $sub_attributes = $attribute->getSubAttributes();
                foreach ($sub_attributes as $sub_attribute) {
                    $sub_item = [];
                    $sub_name = $sub_attribute->getName();
                    $result->$sub_name = $this->_remapNode($sub_node, $sub_attribute);
                }
                break;
            case 'string':
                $result = $this->_convert($this->_path($item, $target, ''), 'string');
                $this->_applyCallback($result, $attribute->getCallback());
                break;
            case 'number':
            case 'boolean':
                $result = $this->_path($item, $target);
                break;
            default:
                $result = $item;
        }
        return $result;
    }

    protected function _remap(array $input): array
    {
        if (!$this->getBackend()->supportsRemapping()) {
            $attributes = $this->getRequest()->getAttributes();
            $array_result = [];
            // Rename attributes
            foreach ($input as $index => $item) {
                $renamed_item = [];
                /**
                 * @var $attribute Attribute
                 */
                foreach ($attributes as $attribute) {
                    $new_name = $attribute->getName();
                    $renamed_item[$new_name] = $this->_remapNode($item, $attribute);
                }
                $array_result[$index] = $renamed_item;
            }
            return $array_result;
        }
        return $input;
    }

    protected function _applyCallback(&$item, $callback)
    {
        if ($callback) {
            if (is_array($item)) {
                array_walk($item, $callback);
            } elseif (is_string($item)) {
                if (substr($callback, 0, 1) === "=") { // Call by value
                    $callback = substr($callback, 1);
                    $item = $callback($item);
                } else { // Call by reference
                    $callback($item);
                }
            }
        }
    }

    protected function _convert($data, $to)
    {
        switch ($to) {
            case 'string':
                if (is_array($data) && isset($data[0])) {
                    return $data[0];
                }
                break;
            case 'array':
                if (is_string($data)) {
                    return array($data);
                }
                break;
        }
        return $data;
    }

    public function getCollection(Request $request)
    {
        $logger = $this->logger;
        $context = ['resource_name' => $this->name];
        try {
            $results = $this->_getResources($request);
        } catch (\Exception $e) {
            $logger->error('Caught exception: ' . $e->getMessage());
            throw $e;
        }
        if ($results instanceof Error) {
            $logger->error("Returning: $results->status $results->title Detail: $results->detail");
            return $results;
        } elseif ($results instanceof \Traversable) {
            $logger->info('[%resource_name%] Received iterator', $context);
        } elseif (is_array($results)) {
            $context['result_count'] = count($results);
            $logger->info('[%resource_name%] Resources retrieved:[%result_count%]', $context);
        }
        return $results;
    }

    public function getResource(Request $request)
    {
        $results = $this->getCollection($request);
        if ($results instanceof Error) {
            return $results;
        }
        $result = $this->_first($results);
        if (!$result) {
            return new Error\NotFound("[$this->name] Not found");
        }
        return $result;
    }

    public function post(Request $request)
    {
        $request->setMethod('POST');
        $this->validateBody($request);
        $result = $this->getBackend()->post($request);
        if ($result instanceof Error) {
            $this->logger->warning("Returning: $result->status $result->title Detail: $result->detail");
        } elseif (is_array($result) && isset($result[0])) {
            $this->setAttributes($request);
            $this->setRequest($request);
            // if it is an array of objects convert the objects into associative arrays
            $result = $this->to_array($result);
            $result = $this->_remap($result);
            $result = $this->_filter($result);
            $result = $this->_first($result);
        }
        return $result;
    }

    public function put(Request $request)
    {
        $request->setMethod('PUT');
        $this->validateBody($request);
        $result = $this->getBackend()->put($request);
        if ($result instanceof Error) {
            $this->logger->warning("Returning: $result->status $result->title Detail: $result->detail");
        }
        return $result;
    }

    public function patch(Request $request)
    {
        $request->setMethod('PATCH');
        $this->validateBody($request);
        $result = $this->getBackend()->patch($request);
        if ($result instanceof Error) {
            $this->logger->warning("Returning: $result->status $result->title Detail: $result->detail");
        }
        return $result;
    }

    public function delete(Request $request)
    {
        $request->setMethod('DELETE');
        $request = $this->prepareRequest($request);
        return $this->getBackend()->delete($request);
    }

    protected function validateConditions(Request $request): void
    {
        $conditions = $request->getConditions();
        foreach ($conditions as $condition) {
            if ($condition->getVariable()->hasSchema()) {
                $schema = $condition->getVariable()->getSchema();
                $values = $condition->getValues();
                foreach ($values as $value) {
                    if ($schema->type === 'integer') {
                        $value = intval($value);
                    }
                    if ($schema->type === 'number') {
                        $value = floatval($value);
                    }
                    try {
                        $value = $schema->in($value);
                    } catch (\Exception $e) {
                        $this->logger->warning("Bad Request: " . $e->getMessage());
                        throw new ApiException(new Error\BadRequest("Invalid request parameters"));
                    }
                }
            }
        }
        return;
    }

    protected function validateBody(Request $request): void
    {
        $json_schema = $this->getConfig()->path('handler.data.schema', false);
        if ($json_schema instanceof Config) {
            $method = $request->getMethod();
            $json_schema = $json_schema->get($method, false);
        }
        if ($json_schema === false) {
            // No schema defined, accepting anything
            return;
        }
        $json_schema = json_decode($json_schema);
        if (!$json_schema) {
            $this->logger->error("Invalid schema defined!");
            throw new ApiException(new Error\InternalServerError("Invalid schema defined"));
        }

        $data = $request->getData();
        $schema = Schema::import($json_schema);
        try {
            $data = $schema->in($data);
            $data = json_decode(json_encode($data));
            $request->setData($data);
        } catch (\Exception $e) {
            $this->logger->warning("Bad Request: " . $e->getMessage());
            throw new ApiException(new Error\BadRequest("Bad $this->name record"));
        }
    }

    private function to_array($data)
    {
        //only process if it's an object or array being passed to the function
        if(is_object($data) || is_array($data)) {
            $ret = (array) $data;
            foreach($ret as &$item) {
                //recursively process EACH element regardless of type
                $item = $this->to_array($item);
            }
            return $ret;
        }
        //otherwise (i.e. for scalar values) return without modification
        else {
            return $data;
        }
    }

    public function getBackend(): BackendInterface
    {
        return $this->backend;
    }

    public function setBackend(BackendInterface $backend)
    {
        $this->backend = $backend;
    }

    public function getConfig(): Config
    {
        return $this->config;
    }

    public function setConfig(Config $config)
    {
        $this->config = $config;
    }

    public function getRequest(): Request
    {
        return $this->request;
    }

    public function setRequest(Request $request): Handler
    {
        $this->request = $request;
        return $this;
    }

    public function getEventsManager(): ?Manager
    {
        return $this->events_manager;
    }

    public function setEventsManager(?Manager $events_manager): Handler
    {
        $this->events_manager = $events_manager;
        return $this;
    }

    public function addFilter(Request\Condition $condition): void
    {
        $this->local_parameters[] = $condition;
    }

    public function clearFilter(): void
    {
        $this->local_parameters = [];
    }

    public function getFilter(): array
    {
        return $this->local_parameters;
    }
}
