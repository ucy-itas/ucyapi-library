<?php
namespace Iss\Api;

use Phalcon\Di\{Di, DiInterface};

trait InjectableTrait
{
    protected ?DiInterface $_dependencyInjector = null;

    public function setDI(DiInterface $dependencyInjector): void
	{
		$this->_dependencyInjector = $dependencyInjector;
	}

    public function getDI(): DiInterface
	{
        $dependencyInjector = $this->_dependencyInjector;
		if (!$dependencyInjector instanceof DiInterface) {
            $dependencyInjector = Di::getDefault();
		}
		return $dependencyInjector;
	}

	/**
     * Magic method __get
     */
	public function __get(string $propertyName)
	{
		$dependencyInjector = $this->_dependencyInjector;
		if (!$dependencyInjector instanceof DiInterface) {
            $dependencyInjector = Di::getDefault();
            if (!$dependencyInjector instanceof DiInterface) {
                throw new Exception("A dependency injection object is required to access the application services");
            }
		}

		/**
         * Fallback to the PHP userland if the cache is not available
         */
		if ($dependencyInjector->has($propertyName)) {
            $service = $dependencyInjector->getShared($propertyName);
			$this->{$propertyName} = $service;
			return $service;
		}

		if ($propertyName == "di") {
            $this->{"di"} = $dependencyInjector;
			return $dependencyInjector;
		}

		/**
         * Accessing the persistent property will create a session bag on any class
         */
		if ($propertyName == "persistent") {
            $persistent = $dependencyInjector->get("sessionBag", [get_class($this)]);
            $this->{"persistent"} = $persistent;
			return $persistent;
		}

		/**
         * A notice is shown if the property is not defined and isn't a valid service
         */
		trigger_error("Access to undefined property $propertyName");
		return null;
	}
}