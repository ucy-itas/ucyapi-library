<?php

function is_ucy_id($value)
{
    if (is_string($value)) {
        if (preg_match('/^010001[0-9]{7}$/', $value) === 1 ||
            preg_match('/^UC[0-9]{7}$/', $value) === 1) {
            return true;
        }
    }
    return false;
}

function is_card_number($value)
{
    if (is_string($value)) {
        if (preg_match('/^[0-9]{10,13}$/', $value) === 1) {
            return true;
        }
    }
    return false;
}

function is_term_code($value)
{
    if (is_string($value)) {
        if (preg_match('/^[0-9]{6}$/', $value) === 1) {
            return true;
        }
    }
    return false;
}

function is_course_code($value)
{
    if (is_string($value)) {
        if (preg_match('/^[0-9]{4,5}$/', $value) === 1) {
            return true;
        }
    }
    return false;
}

function is_student_id($value)
{
    if (is_numeric($value)) {
        if (preg_match('/^[0-9]{6,7}$/', $value) === 1) {
            return true;
        }
    }
    return false;
}

function is_base64($value)
{
    if (is_string($value)) {
        if (preg_match('/^(?:[A-Za-z0-9+\/]{4})*(?:[A-Za-z0-9+\/]{2}==|[A-Za-z0-9+\/]{3}=)?$/', $value) === 1) {
            return true;
        }
    }
    return false;
}
function isUuid( $uuid ) {
    if (!is_string($uuid) || (preg_match('/^[a-f\d]{8}(-[a-f\d]{4}){4}[a-f\d]{8}$/i', $uuid) !== 1)) {
        return false;
    }
    return true;
}
