<?php
namespace Iss\Api;

interface ChainOfResponsibilityInterface
{
    public function setSuccessor(ChainOfResponsibilityInterface $successor): ChainOfResponsibilityInterface;

    public function appendSuccessor(ChainOfResponsibilityInterface $successor): ChainOfResponsibilityInterface;

    public function getSuccessor(): ?ChainOfResponsibilityInterface;

    public function execute(array $request);
}