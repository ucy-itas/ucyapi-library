<?php
namespace Iss\Api;

trait ArrayAccessTrait
{
    private array $_context = [];

    public function offsetExists($offset): bool
    {
        return isset($this->_context[$offset]);
    }

    public function offsetGet($offset): mixed
    {
        return isset($this->_context[$offset]) ? $this->_context[$offset] : null;
    }

    public function offsetSet($offset, $value): void
    {
        if (is_null($offset)) {
            $this->_context[] = $value;
        } else {
            $this->_context[$offset] = $value;
        }
    }

    public function offsetUnset($offset): void
    {
        unset($this->_context[$offset]);
    }
}
