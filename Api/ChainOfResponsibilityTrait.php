<?php
namespace Iss\Api;

trait ChainOfResponsibilityTrait
{
    private ?ChainOfResponsibilityInterface $successor = null;

    public function setSuccessor(ChainOfResponsibilityInterface $successor): ChainOfResponsibilityInterface
    {
        $this->successor = $successor;
        return $this;
    }

    public function appendSuccessor(ChainOfResponsibilityInterface $successor): ChainOfResponsibilityInterface
    {
        if (is_null($this->successor)) {
            return $this->setSuccessor($successor);
        } else {
            return $this->successor->appendSuccessor($successor);
        }
    }

    public function getSuccessor() : ?ChainOfResponsibilityInterface
    {
        return $this->successor;
    }

    public function execute(array $request)
    {
        $processed = $this->process($request);
        if ($processed === null) {
            // the request has not been processed by this handler => see the next
            if ($this->successor !== null) {
                $processed = $this->successor->execute($request);
            }
        }
        return $processed;
    }

    abstract protected function process(array $request);
}