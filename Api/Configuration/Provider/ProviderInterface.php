<?php
namespace Iss\Api\Configuration\Provider;

use Phalcon\Config\Config;

interface ProviderInterface
{
    public function __invoke(string $key, ?string $path = null): Config;

    public function get(string $key, ?string $path = null): Config;
}