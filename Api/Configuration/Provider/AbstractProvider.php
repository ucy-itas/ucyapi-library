<?php
namespace Iss\Api\Configuration\Provider;

use Iss\Api\ChainOfResponsibilityInterface;
use Iss\Api\ChainOfResponsibilityTrait;
use Phalcon\Config\Config;

abstract class AbstractProvider implements ChainOfResponsibilityInterface, ProviderInterface
{
    use ChainOfResponsibilityTrait;

    public function __invoke(string $key, ?string $path = null): Config
    {
        return $this->get($key, $path);
    }
}