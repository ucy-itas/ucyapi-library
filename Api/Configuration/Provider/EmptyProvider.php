<?php
namespace Iss\Api\Configuration\Provider;

use Iss\Api\ChainOfResponsibilityInterface;
use Iss\Api\ChainOfResponsibilityTrait;
use Phalcon\Config\Config;

class EmptyProvider extends AbstractProvider
{
    use ChainOfResponsibilityTrait;

    protected function process(array $request)
    {
        if (is_null($this->getSuccessor())) {
            return new Config();
        } else {
            return null;
        }
    }

    public function get(string $key, ?string $path = null): Config
    {
        $result = $this->execute([$key, $path]);
        if (is_null($result)) {
            return new Config();
        }
        return $result;
    }

    public function execute(array $request)
    {
        $result = parent::execute($request);
        if (is_null($result)) {
            return new Config();
        }
        return $result;
    }

}