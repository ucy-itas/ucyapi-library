<?php
namespace Iss\Api\Configuration\Provider;

use Phalcon\Config\{Config, ConfigFactory};

class File extends AbstractProvider
{
    protected array $_configurations = [];

    protected string $_configurations_location;

    protected string $_extensions = 'ini,json,php,yaml';

    protected ConfigFactory $_configuration_factory;

    public function __construct(string $default_location)
    {
        $this->_configurations_location = $default_location;
        $this->_configuration_factory = new ConfigFactory();
        $this->setSuccessor(new EmptyProvider());
    }

    public function get(string $key, ?string $path = null): Config
    {
        $config = $this->execute([$key, $path]);
        if (is_null($config)) {
            // The entire chain of responsibility has returned null
            return new Config();
        }
        return $config;
    }

    protected function process(array $request)
    {
        [$key, $path] = $request;
        return $this->getConfiguration($key, $path);
    }

    public function getExtensions(): string
    {
        return $this->_extensions;
    }

    public function setExtensions(string $extensions): void
    {
        $this->_extensions = $extensions;
    }

    private function getConfiguration(string $key, ?string $path = null): ?Config
    {
        $configuration_key = $key . '@' . ($path ? $path : 'config');
        if (isset($this->_configurations[$configuration_key])) {
            return $this->_configurations[$configuration_key];
        }

        $configuration = $this->loadFile($key, $path);
        if (!is_null($configuration)) {
            $this->_configurations[$configuration_key] = $configuration;
            $this->resolveReferences($configuration);
        }
        return $configuration;
    }

    private function loadFile(string $filename, ?string $path = null): ?Config
    {
        if (is_null($path)) {
            $files = glob( "{$this->_configurations_location}/$filename.{{$this->_extensions}}", GLOB_BRACE);
        } else {
            $files = glob("$path/$filename.{{$this->_extensions}}", GLOB_BRACE);
        }
        if (count($files) === 0) {
            $replacements = -1;
            // Reverse the string because I want to make replacements from the end.
            // In case a requested config is a/b/c the check order would be
            // a/b/c => a/b.c => a.b.c
            $filename = strrev($filename);
            $filename = preg_replace('/\//', '.', $filename, 1, $replacements);
            $filename = strrev($filename);
            if ($replacements === 0) {
                return null;
            } else {
                return $this->loadFile($filename, $path);
            }
        }
        foreach ($files as $file) {
            $configuration = $this->_configuration_factory->load($file);
            return $configuration;
        }
        return null;
    }

    private function resolveReferences(Config $config, ?Config $initial_config = null)
    {
        if (is_null($initial_config)) {
            $initial_config = $config;
        }
        foreach ($config as $key => $member) {
            if ($key === '$ref') {
                $parts = explode('#', $member);
                if (count($parts) === 1) {
                    $parts[] = null;
                }
                if (count($parts) === 2) {
                    list($file, $path) = $parts;
                    $location = null;
                    if ($file) {
                        $referenced_part = $this->getConfiguration($file, $location);
                        if ($path) {
                            $referenced_part = $referenced_part->path($path, new Config(), '/');
                        }
                    } else {
                        $referenced_part = $initial_config->path($path, new Config(), '/');
                    }
                    unset($config->$key);
                    if ($referenced_part instanceof Config) {
                        /**
                         * $referenced_part is the part in the top level files which are often common.
                         * If common attributes exist then the attributes from $config should overwrite the ones in
                         * $referenced_part but $referenced_part should not be changed. As a result a reverse merge
                         * is executed on a clone of the $referenced_part to overwrite common attributes and then a regular
                         * merge is performed on the result.
                         * $config is more specific and $referenced_part is more broad.
                         */
                        $cloned_part = new Config($referenced_part->toArray());
                        $cloned_part->merge($config);
                        $config->merge($cloned_part);
                    }
                }
            } else {
                if ($member instanceof Config) {
                    $this->resolveReferences($member, $initial_config);
                }
            }
        }
    }
}