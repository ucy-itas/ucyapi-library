<?php
namespace Iss\Api;

use Phalcon\Mvc\Micro;

interface ServiceInterface
{
    public function register(Micro $application): bool;

    public function unregister(Micro $application): ?ServiceInterface;

    public static function getName(): string;

    public function getPriority(): int;

    public function setPriority(int $priority): ServiceInterface;

    public function setPriorityBefore(ServiceInterface $service): ServiceInterface;

    public function setPriorityAfter(ServiceInterface $service): ServiceInterface;
}