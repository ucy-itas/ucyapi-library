<?php
namespace Iss\Api\Authentication;

class Principal extends \ArrayObject
{
    public function __construct(array $values = []) {
        $values = array_merge(
            array_fill_keys(['name','ucy_id','id','ip','expiration','provider'], null),
            $values
        );
        parent::__construct($values);
    }
}